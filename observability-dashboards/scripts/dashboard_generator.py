#!/usr/bin/env python3
import json
import os
import yaml
import argparse


def load_template(template_path):
    """
    Load and parse the JSON template from the specified file.
    """
    with open(template_path, "r") as file:
        return json.load(file)


def load_variables(vars_path):
    """
    Load variables from YAML or JSON file.
    """
    with open(vars_path, "r") as file:
        return yaml.safe_load(file)


def generate_dashboard(template, variables, output_path):
    """
    Generate a Grafana dashboard by replacing placeholders in the JSON template with the provided variables.
    """
    # Flatten nested variables
    def flatten_dict(d, parent_key='', sep='_'):
        items = []
        for k, v in d.items():
            new_key = f"{parent_key}{sep}{k}" if parent_key else k
            
            if isinstance(v, dict):
                items.extend(flatten_dict(v, new_key, sep=sep).items())
            else:
                items.append((new_key, v))
        return dict(items)

    flat_vars = flatten_dict(variables)

    # Convert template to string for replacement
    template_str = json.dumps(template)

    # Replace placeholders
    for key, value in flat_vars.items():
        template_str = template_str.replace(f"{{{{ {key} }}}}", str(value))

    # Parse back to JSON
    updated_template = json.loads(template_str)

    # Ensure the output directory exists
    output_dir = os.path.dirname(output_path)
    os.makedirs(output_dir, exist_ok=True)

    # Write the updated dashboard to the output file
    with open(output_path, "w") as file:
        json.dump(updated_template, file, indent=2)
    print(f"Dashboard saved to: {output_path}")


def main():
    parser = argparse.ArgumentParser(description="Generate Grafana Dashboards")
    parser.add_argument("--template", required=True, help="Path to the dashboard template (JSON format)")
    parser.add_argument("--vars", required=True, help="Path to variables file (YAML or JSON)")
    parser.add_argument("--output", required=True, help="Path to save the generated dashboard")

    args = parser.parse_args()

    # Load the JSON template
    template = load_template(args.template)

    # Load variables
    variables = load_variables(args.vars)

    # Generate the dashboard
    generate_dashboard(template, variables, args.output)


if __name__ == "__main__":
    main()