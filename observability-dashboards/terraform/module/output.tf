output "dashboard_url" {
  value = grafana_dashboard.dashboard.url
  description = "The URL of the deployed Grafana dashboard"
}