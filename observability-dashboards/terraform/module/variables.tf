variable "market" {
  type        = string
  description = "Market name"
}

variable "app_name" {
  type        = string
  description = "Application name"
}

variable "grafana_url" {
  type        = string
  description = "Grafana URL"
}

variable "grafana_token" {
  type        = string
  description = "Grafana API token"
  sensitive   = true
}

variable "dashboard_config" {
  type        = string
  description = "Grafana dashboard configuration JSON"
}