terraform {
  required_providers {
    grafana = {
      source  = "grafana/grafana"
      version = "~> 1.40.1"
    }
  }
}

provider "grafana" {
  url  = var.grafana_url
  auth = var.grafana_token
}

# Create market folder
resource "grafana_folder" "market_folder" {
  title = var.market

  lifecycle {
    # Ignore changes to the folder ID if it already exists
    ignore_changes = [id]
  }
}

# Deploy Grafana dashboard
resource "local_file" "dashboard_config" {
  content  = var.dashboard_config
  filename = "dashboard.json"
}

resource "grafana_dashboard" "dashboard" {
  folder      = grafana_folder.market_folder.id
  config_json = file(local_file.dashboard_config.filename)

  lifecycle {
    # Ignore changes to the UID if the dashboard already exists
    ignore_changes = [uid]
  }
}